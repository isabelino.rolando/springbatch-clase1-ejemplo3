package com.spring.batchparallel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchParallelApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchParallelApplication.class, args);
	}

}
