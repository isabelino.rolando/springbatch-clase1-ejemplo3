package com.spring.batchparallel.config;

import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;


@Configuration
@EnableBatchProcessing
public class BatchConfig {
	
	Logger logger = (Logger) LoggerFactory.getLogger(BatchConfig.class);
	 
    @Autowired
    public JobBuilderFactory jobBuilderFactory;
 
    @Autowired
    public StepBuilderFactory stepBuilderFactory;
 
    private TaskletStep taskletStep(String step) {
        return stepBuilderFactory.get(step).tasklet((contribution, chunkContext) -> {
            IntStream.range(1, 10).forEach(token -> logger.info("Step:" + step + " token:" + token));
            return RepeatStatus.FINISHED;
        }).build();
 
    }
 
 
    @Bean
    public Job parallelStepsJob() {
 
        Flow masterFlow = (Flow) new FlowBuilder("masterFlow").start(taskletStep("step1")).build();
 
 
        Flow flowJob1 = (Flow) new FlowBuilder("flow1").start(taskletStep("step2")).build();
        Flow flowJob2 = (Flow) new FlowBuilder("flow2").start(taskletStep("step3")).build();
        Flow flowJob3 = (Flow) new FlowBuilder("flow3").start(taskletStep("step4")).build();
 
        Flow slaveFlow = (Flow) new FlowBuilder("slaveFlow")
                .split(new SimpleAsyncTaskExecutor()).add(flowJob1, flowJob2, flowJob3).build();
 
        return (jobBuilderFactory.get("parallelFlowJob")
                .incrementer(new RunIdIncrementer())
                .start(masterFlow)
                .next(slaveFlow)
                .build()).build();
 
    }

}
